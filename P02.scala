object main {
  def penultimate[A](l : List[A]): A = l match {
    case x :: _ :: Nil => x
    case _ :: xs => penultimate(xs)
    case Nil => throw new NoSuchElementException
  }

  def main(args: Array[String]): Unit =
    println(penultimate(List(1, 1, 2, 3, 5, 8)))
}
