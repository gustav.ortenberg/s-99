object Main{
  def length[A](l: List[A]): Int = l match{
    case Nil => 0
    case _ :: xs => 1 + length(xs)
  }
  def main(Args: Array[String]): Unit =
    println(length(List(1, 1, 2, 3, 5, 8)))
}
