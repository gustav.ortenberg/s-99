object main {
  def last[A](l : List[A]): A = l match {
    case Nil => throw new NoSuchElementException
    case x :: Nil => x
    case _ :: xs => last(xs)
  }

  def main(args: Array[String]): Unit =
    println(last(List(1, 1, 2, 3, 5, 8)))
}
