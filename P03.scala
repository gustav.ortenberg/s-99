object Main {
  def nth[A](n: Int, l: List[A]): A = n match{
    case 0 => l.head
    case x => nth(x-1, l.tail)
  }
  def main(Args: Array[String]): Unit =
    println(nth(2, List(1, 1, 2, 3, 5, 8)))
}
