object Main{
  def reverse[A](l: List[A]): List[A] = l match{
    case Nil => Nil
    case x :: xs => reverse(xs) :+ x
  }
  def main(Args: Array[String]): Unit =
    println(reverse(List(1,1,2,3,5,8)))
}
